package com.cognizant.springlearn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import com.cognizant.springlearn.dao.Employee;
import com.cognizant.springlearn.service.EmployeeService;

public class EmployeeController {
	
	EmployeeService service=new EmployeeService();

	@GetMapping("/employees")
	public List<Employee> getAllEmployee() {

		return service.getAllEmployee();
	}

}
