package com.cognizant.springlearn.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.springlearn.dao.Employee;
import com.cognizant.springlearn.dao.EmployeeDao;

	@Service
	public class EmployeeService{
		
		
		EmployeeDao dao;
		
		@Transactional
		public List<Employee> getAllEmployee() {
			return dao.getAllEmployee();
		}
}
