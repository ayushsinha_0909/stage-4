package com.cognizant.springlearn.dao;

import java.util.List;

public class EmployeeDao {

	private List<Employee> employeeList;

	public EmployeeDao(List<Employee> employeeList) {
		super();
		this.employeeList = employeeList;
	}

	public List<Employee> getAllEmployee() {
		return employeeList;
	}

	@Override
	public String toString() {
		return "EmployeeDao [employeeList=" + employeeList + "]";
	}
	
	

}
