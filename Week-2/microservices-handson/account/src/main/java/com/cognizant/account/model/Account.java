package com.cognizant.account.model;

public class Account {

	private long acnumber;
	private String actype;
	private long acbalance;

	public long getAcnumber() {
		return acnumber;
	}

	public void setAcnumber(long acnumber) {
		this.acnumber = acnumber;
	}

	public String getActype() {
		return actype;
	}

	public void setActype(String actype) {
		this.actype = actype;
	}

	public long getAcbalance() {
		return acbalance;
	}

	public void setAcbalance(long acbalance) {
		this.acbalance = acbalance;
	}

	public Account(long acnumber, String actype, long acbalance) {
		super();
		this.acnumber = acnumber;
		this.actype = actype;
		this.acbalance = acbalance;
	}

	@Override
	public String toString() {
		return "Account [acnumber=" + acnumber + ", actype=" + actype + ", acbalance=" + acbalance + "]";
	}

}