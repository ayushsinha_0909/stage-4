package com.cognizant.account.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.account.model.Account;

@RestController
public class AccountController {
	static ArrayList<Account> account = new ArrayList<>();
	static {

		account.add(new Account(12345678, "saving", 234343));
		account.add(new Account(87654321, "saving", 250000));
		account.add(new Account(10987654, "saving", 254789));
	}

	@GetMapping("/accounts/{number}")
	public Account getAcctByNum(@PathVariable long number) {
		for (Account a : account) {
			if (a.getAcnumber() == number)
				return a;
		}

		return null;
	}

}
