package com.cognizant.loan.model;

public class Loan {

	private long loanNumber;
	private String type;
	private long loan;
	private long emi;
	private int tenure;

	public Loan(long loanNumber, String type, long loan, long emi, int tenure) {
		super();
		this.loanNumber = loanNumber;
		this.type = type;
		this.loan = loan;
		this.emi = emi;
		this.tenure = tenure;
	}

	@Override
	public String toString() {
		return "Loan [loanNumber=" + loanNumber + ", type=" + type + ", loan=" + loan + ", emi=" + emi + ", tenure="
				+ tenure + "]";
	}

	public long getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(long loanNumber) {
		this.loanNumber = loanNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getLoan() {
		return loan;
	}

	public void setLoan(long loan) {
		this.loan = loan;
	}

	public long getEmi() {
		return emi;
	}

	public void setEmi(long emi) {
		this.emi = emi;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
}