package com.cognizant.loan.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.loan.model.Loan;

@RestController
public class LoanController {
	public static ArrayList<Loan> loan = new ArrayList<>();
	static {
		loan.add(new Loan(98765, "car", 500000, 8000, 18));
		loan.add(new Loan(54321, "home", 7000000, 12000, 12));
		loan.add(new Loan(10987, "bike", 60000, 2500, 18));
	}

	@GetMapping("/loans/{number}")
	public Loan getLoanById(@PathVariable long number) {
		for (Loan ln : loan) {
			if (ln.getLoanNumber() == number) {
				return ln;
			}
		}
		return null;
	}

}
